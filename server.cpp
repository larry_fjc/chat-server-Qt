#include "server.h"
#include "QtWebSockets/qwebsocketserver.h"
#include "QtWebSockets/qwebsocket.h"
#include <QtCore/QDebug>

QT_USE_NAMESPACE

/**
 * @brief Server::Server
 * @param port
 * @param parent
 */
Server::Server(quint16 port, QObject *parent) :
    QObject(parent),
    m_miWebSocketServer(new QWebSocketServer(QStringLiteral("Chat server"),
                                             QWebSocketServer::NonSecureMode, this)),
    m_clientes(),
    m_colors(),
    m_historial()
{
    m_colors << "red" << "green" << "blue" << "magenta" << "purple" << "plum" << "orange";
    if(m_miWebSocketServer->listen(QHostAddress::Any, port))
    {
        qDebug() << "Servidor escuchando en el puerto" << port;
        connect(m_miWebSocketServer, &QWebSocketServer::newConnection, this, &Server::onNewConnection);
        connect(m_miWebSocketServer, &QWebSocketServer::closed, this, &Server::closed);
    }
}

/**
 * @brief Server::~Server
 */
Server::~Server()
{
    m_miWebSocketServer->close();
    qDeleteAll(m_clientes.begin(), m_clientes.end());
}

/**
 * @brief Server::onNewConnection
 */
void Server::onNewConnection()
{
    QWebSocket *socketCliente = m_miWebSocketServer->nextPendingConnection();
    m_clientes << socketCliente;
    enviaHistorial(socketCliente);
    connect(socketCliente, &QWebSocket::textMessageReceived, this, &Server::processTextMessage);
    connect(socketCliente, &QWebSocket::disconnected, this, &Server::socketDisconnected);
}

/**
 * @brief Server::processTextMessage
 * @param message
 */
void Server::processTextMessage(QString message)
{
    //qint64 bytes;
    QWebSocket *pSender = qobject_cast<QWebSocket *>(sender());
    if(pSender->objectName().isNull())  //Asigna nombre y color
    {
        pSender->setObjectName(message);
        QJsonObject webBrowserColor;
        QString userColor = m_colors.takeFirst();
        webBrowserColor["type"] = "color";
        webBrowserColor["data"] = userColor;
        m_colors.push_back(userColor);
        QJsonDocument uColor(webBrowserColor);
        userColor = uColor.toJson(QJsonDocument::Compact);
        pSender->sendTextMessage(userColor);
    }
    else    //Encapsula mensaje en JSON
    {
        QDateTime time = QDateTime::currentDateTime();
        QStringListIterator it(m_colors);
        QJsonObject jsonObj;
        jsonObj["time"] = time.toString("HH:mm:ss ap");
        jsonObj["text"] =  message;
        jsonObj["author"] = pSender->objectName();
        jsonObj["color"] = it.next();

        //Encapsula mensaje individual
        QJsonObject mssg;
        mssg["type"] = "message";
        mssg["data"] = jsonObj;
        QJsonDocument doc(mssg);
        QString strJson(doc.toJson(QJsonDocument::Compact));

        //Añade mensaje al historial
        m_historial.append(jsonObj);

        //enviando mensaje
        Q_FOREACH (QWebSocket *pClient, m_clientes)
        {
            pClient->sendTextMessage(strJson);
        }
    }
}

void Server::enviaHistorial(QWebSocket *nuevoCte)
{
    QJsonObject history;
    history["type"] = "history";
    history["data"] = m_historial;
    QJsonDocument doc(history);
    QString strHistory(doc.toJson(QJsonDocument::Compact));
    nuevoCte->sendTextMessage(strHistory);
}

void Server::socketDisconnected()
{
    QWebSocket *pClient = qobject_cast<QWebSocket *>(sender());
    qDebug() << "socketDisconnected:" << pClient;
    if (pClient) {
        m_clientes.removeAll(pClient);
        pClient->deleteLater();
    }
}

























