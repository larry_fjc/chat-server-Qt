#ifndef SERVER
#define SERVER

#include <QtCore/QObject>
#include <QtCore/QList>
#include <QtCore/QByteArray>
#include <QJsonObject>
#include <QJsonDocument>
#include <QJsonArray>


QT_FORWARD_DECLARE_CLASS(QWebSocketServer)
QT_FORWARD_DECLARE_CLASS(QWebSocket)

class Server : public QObject
{
    Q_OBJECT
public:
    explicit Server(quint16 port, QObject *parent = Q_NULLPTR);
    void enviaHistorial(QWebSocket *nuevoCte);
    ~Server();

signals:
    void closed();

private slots:
    void onNewConnection();
    void processTextMessage(QString message);
    void socketDisconnected();

private:
    QWebSocketServer *m_miWebSocketServer;
    QList<QWebSocket *> m_clientes;
    QStringList m_colors;
    QJsonArray m_historial;

};


#endif // SERVER

